filter_packages = [
    ("^(BaseOS|AppStream|HighAvailability|NFV|RT|ResilientStorage)$", {
        "*": [
            "centos-linux-repos", # We want to force-exclude centos-linux repos from the resolver otherwise this lands in BaseOS and centos-stream-repos lands in AppStream
            "python36",
            "gcc-toolset-9-*-testsuite",
            "gcc-toolset-9-gcc-plugin-devel",
            "tuned-profiles-sap",
            "tuned-profiles-sap-hana",
            "java-*slowdebug*",
            "java-*fastdebug*",
        ]
    }),

    ("^BaseOS$", {
        "*": [
            "compat-openssl10-devel",
            "compat-openssl10-pkcs11-helper",
            "openldap-servers-debuginfo",
            "ongres-scram",
        ]
    }),

    ("^RT$", {
        "*": [
            "kernel-rt-kvm-debuginfo",
            "kernel-rt-debug-kvm-debuginfo",
        ]
    }),

    ("^.*$", {
        "*": [
            "kernel-*-internal",
            "kpatch-patch-*-internal",
            "scap-security-guide-ansible-tasks",
        ]
    }),
]

additional_packages = [
    ("^AppStream$", {
        "*": [
            "libasan5",
            "libubsan1",
            "dotnet5.0",
            "gcc-toolset-10-dyninst-devel",
            "gnome-software-editor",
            "pipewire-docs",
            "rt-tests",
            "micropipenv",
            "nispor",
            "nispor-devel",
            "python3-nispor",
            "libnumbertext",
            "dotnet-sdk-3.0",
            "perl-IO-String",
            "stalld",
            "dejavu-lgc-sans-fonts",
            "xorg-x11-drivers",
            "fstrm",
            "fstrm-devel",
            "ucx-cma",
            "ucx-ib",
            "ucx-rdmacm"
            "ucx-devel",
            "mpich-doc",
            "mvapich2-devel",
            "mvapich2-psm2-devel",
            "mvapich2-doc",
            "libecpg",
            "rsyslog-udpspoof",
            "mysql-selinux",
            "pg_repack",
            "rshim",
            "qatlib",
            "tracer",
            "flatpak-xdg-utils",
            "modulemd-tools",
            "emoji-picker",
            "gcc-toolset-11",
            "xapian-core",
            "gnome-session-kiosk-session",
            "udftools",
            "qperf",
            "gcc-toolset-11-dwz",
            "gcc-toolset-11-gcc",
            "ansible-freeipa-tests",
            "xorg-x11-server-Xwayland",
            "gcc-toolset-11-strace",
            "ansible-pcp",
        ]
    }),
    ("^BaseOS$", {
        "*": [
            "centos-stream-repos",
            "python3-debuginfo",
            "python3-pyverbs",
            "syslinux-tftpboot",
            "samba-winexe",
            "accel-config",
            "ima-evm-utils0",
        ]
    }),

    ("^BaseOS$", {
        "x86_64": [
            # Keep alsa-sof-firmware synchronized with
            # alsa-sof-firmware in the comps file
            "alsa-sof-firmware-debug",
        ],
    }),

    ("^BaseOS$", {
       "aarch64": [
            "opencsd",
        ],
    }),

    ("^Buildroot$", {
        "*": [
            "*",
        ]
    }),

    ("^PowerTools$", {
        "*": [
            "libdnf-devel",
            "librepo-devel",
            "librhsm-devel",
            "libsolv-devel",
            "libsolv-tools",
            "ibus-typing-booster-tests",
            "dotnet5.0-build-reference-packages",
            "dotnet-sdk-3.1-source-built-artifacts",
            "dotnet-sdk-5.0-source-built-artifacts",
        ]
    }),

    ("^PowerTools$", {
        "aarch64": [
            "java-1.8.0-openjdk-*slowdebug*",
            "java-11-openjdk-*slowdebug*",
        ],
        "ppc64le": [
            "java-1.8.0-openjdk-*slowdebug*",
            "java-11-openjdk-*slowdebug*",
        ],
        "x86_64": [
            "java-1.8.0-openjdk-*slowdebug*",
            "java-11-openjdk-*slowdebug*",
        ],
    }),
    ("^PowerTools$", {
        "x86_64": [
            "java-1.8.0-openjdk-*fastdebug*",
            "java-11-openjdk-*fastdebug*",
        ]
    }),
]
